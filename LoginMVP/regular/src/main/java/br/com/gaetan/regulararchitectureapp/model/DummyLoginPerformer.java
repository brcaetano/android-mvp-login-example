package br.com.gaetan.regulararchitectureapp.model;

import android.os.Handler;

public class DummyLoginPerformer implements LoginPerformer {

    private Handler mHandler;
    private String mEmail, mPassword;
    private OnLoginListener mListener;

    @Override
    public void performLogin(String email, String password, OnLoginListener listener) {

        mHandler = new Handler();

        mEmail = email;
        mPassword = password;
        mListener = listener;

        // Faz o login em outra thread com um delay de 2 segundos
        new Thread() {
            @Override
            public void run() {
                performLoginWithDelay(2000);
            }
        }.start();
    }

    private void performLoginWithDelay(int delayMs) {
        sleep(delayMs);
        mHandler.post(mLoginResponse);
    }

    // Resposta do login
    private Runnable mLoginResponse = new Runnable() {
        @Override
        public void run() {
            if ("admin".equals(mPassword) && "admin@admin.com".equals(mEmail)) {
                mListener.onLoginSuccess();
            } else {
                mListener.onLoginFailure();
            }
        }
    };

    // Delay
    private void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}