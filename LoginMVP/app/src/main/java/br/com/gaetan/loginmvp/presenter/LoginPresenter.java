package br.com.gaetan.loginmvp.presenter;

import java.lang.ref.WeakReference;

import br.com.gaetan.loginmvp.model.DummyLoginPerformer;
import br.com.gaetan.loginmvp.model.LoginPerformer;
import br.com.gaetan.loginmvp.model.OnLoginListener;
import br.com.gaetan.loginmvp.view.LoginView;

public class LoginPresenter implements OnLoginListener {

    private final WeakReference<LoginView> mLoginView;

    private LoginPerformer mLoginPerformer;

    public LoginPresenter(LoginView loginView) {
        mLoginView = new WeakReference<>(loginView);
        mLoginPerformer = new DummyLoginPerformer();
    }

    private LoginView getView() {
        return mLoginView.get();
    }

    public void performLogin(String email, String password) {
        getView().showLoading();
        mLoginPerformer.performLogin(email, password, this);
    }

    @Override
    public void onLoginSuccess() {
        getView().hideLoading();
        getView().showSuccess();
    }

    @Override
    public void onLoginFailure() {
        getView().hideLoading();
        getView().showError();
    }
}
