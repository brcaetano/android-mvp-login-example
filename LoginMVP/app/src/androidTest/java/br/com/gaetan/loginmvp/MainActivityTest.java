package br.com.gaetan.loginmvp;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testLoginWithSuccess() throws Exception {
        onView(withId(R.id.user_email)).perform(typeText("admin@admin.com"));
        onView(withId(R.id.user_password)).perform(typeText("admin"));
        onView(withId(R.id.login)).perform(click());
        onView(withText(R.string.login_success)).check(matches(isDisplayed()));
    }

    @Test
    public void testLoginWithFailure() throws Exception {
        onView(withId(R.id.user_email)).perform(typeText("invalid@invalid.com"));
        onView(withId(R.id.user_password)).perform(typeText("invalid"));
        onView(withId(R.id.login)).perform(click());
        onView(withText(R.string.login_failure)).check(matches(isDisplayed()));
    }
}
