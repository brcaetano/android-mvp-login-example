package br.com.gaetan.loginmvp.view;

public interface LoginView {
    void showLoading();

    void hideLoading();

    void showError();

    void showSuccess();
}
