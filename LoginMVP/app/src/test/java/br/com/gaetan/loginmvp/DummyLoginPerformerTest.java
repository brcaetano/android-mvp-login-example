package br.com.gaetan.loginmvp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import br.com.gaetan.loginmvp.model.DummyLoginPerformer;
import br.com.gaetan.loginmvp.model.OnLoginListener;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@RunWith(JUnit4.class)
public class DummyLoginPerformerTest {

    @Test
    public void testLoginWithSuccess() {
        OnLoginListener listener = mock(OnLoginListener.class);

        DummyLoginPerformer dummyLoginPerformer = new DummyLoginPerformer();
        dummyLoginPerformer.performLogin("admin@admin.com", "admin", listener);

        verify(listener).onLoginSuccess();
        verify(listener, never()).onLoginFailure();
    }

    @Test
    public void testLoginWithError() {
        OnLoginListener listener = mock(OnLoginListener.class);

        DummyLoginPerformer dummyLoginPerformer = new DummyLoginPerformer();
        dummyLoginPerformer.performLogin("invalid@invalid.com", "invalid", listener);

        verify(listener).onLoginFailure();
        verify(listener, never()).onLoginSuccess();
    }
}
