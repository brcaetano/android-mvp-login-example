package br.com.gaetan.regulararchitectureapp;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import br.com.gaetan.regulararchitectureapp.model.DummyLoginPerformer;
import br.com.gaetan.regulararchitectureapp.model.LoginPerformer;
import br.com.gaetan.regulararchitectureapp.model.OnLoginListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.user_email)
    EditText mUserEmail;

    @BindView(R.id.user_password)
    EditText mUserPassword;

    private ProgressDialog mProgressDialog;

    // Componente que faz o login
    private LoginPerformer mLoginPerformer = new DummyLoginPerformer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.login)
    public void onClickLogin() {
        String email = mUserEmail.getText().toString();
        String password = mUserPassword.getText().toString();
        performLogin(email, password);
    }

    private void performLogin(String email, String password) {
        // Lógica de UI
        showLoading();

        // Código de negócio
        mLoginPerformer.performLogin(email, password, new OnLoginListener() {
            @Override
            public void onLoginSuccess() {
                // Lógica de UI
                hideLoading();
                showSuccess();
            }

            @Override
            public void onLoginFailure() {
                // Lógica de UI
                hideLoading();
                showError();
            }
        });
    }

    public void showLoading() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(R.string.loading_wait));
        mProgressDialog.show();
    }

    public void hideLoading() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    public void showError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.login_title);
        builder.setMessage(R.string.login_failure);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    public void showSuccess() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.login_title);
        builder.setMessage(R.string.login_success);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }
}
