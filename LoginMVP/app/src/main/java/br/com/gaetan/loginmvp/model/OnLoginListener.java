package br.com.gaetan.loginmvp.model;

public interface OnLoginListener {
    void onLoginSuccess();

    void onLoginFailure();
}
