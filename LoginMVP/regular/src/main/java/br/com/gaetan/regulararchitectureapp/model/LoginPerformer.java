package br.com.gaetan.regulararchitectureapp.model;

public interface LoginPerformer {

    void performLogin(String email, String password, OnLoginListener listener);

}
