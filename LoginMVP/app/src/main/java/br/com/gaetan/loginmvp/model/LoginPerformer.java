package br.com.gaetan.loginmvp.model;

public interface LoginPerformer {

    void performLogin(String email, String password, OnLoginListener listener);

}
