package br.com.gaetan.loginmvp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import br.com.gaetan.loginmvp.presenter.LoginPresenter;
import br.com.gaetan.loginmvp.view.LoginView;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@RunWith(JUnit4.class)
public class LoginPresenterTest {

    @Test
    public void testLoginWithSuccess() {
        LoginView loginView = mock(LoginView.class);

        // Faz o login com sucesso
        LoginPresenter presenter = new LoginPresenter(loginView);
        presenter.performLogin("admin@admin.com", "admin");

        verify(loginView).showLoading();
        verify(loginView).hideLoading();
        verify(loginView).showSuccess();
        verify(loginView, never()).showError();
    }

    @Test
    public void testLoginWithError() {
        LoginView loginView = mock(LoginView.class);

        // Faz o login com erro
        LoginPresenter presenter = new LoginPresenter(loginView);
        presenter.performLogin("invalid@invalid.com", "invalid");

        verify(loginView).showLoading();
        verify(loginView).hideLoading();
        verify(loginView).showError();
        verify(loginView, never()).showSuccess();
    }

}
