package br.com.gaetan.regulararchitectureapp.model;

public interface OnLoginListener {
    void onLoginSuccess();

    void onLoginFailure();
}
